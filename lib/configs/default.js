const [, ...restrictedGlobals] = require('eslint-config-airbnb-base/rules/variables.js').rules[
  'no-restricted-globals'
];

module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  extends: [
    'airbnb-base',
    'plugin:vue/recommended',
    'prettier',
    'prettier/vue',
    'plugin:promise/recommended',
  ],
  parserOptions: {
    parser: '@babel/eslint-parser',
  },
  plugins: ['@babel', 'unicorn', 'promise', 'import', '@gitlab'],
  rules: {
    camelcase: [
      'error',
      {
        properties: 'never',
        ignoreDestructuring: true,
      },
    ],
    camelcase: 'off',
    'unicorn/filename-case': ['error', { case: 'snakeCase' }],
    'import/prefer-default-export': 'off',
    'import/no-default-export': 'error',
    'import/no-deprecated': 'error',
    'no-implicit-coercion': [
      'error',
      {
        boolean: true,
        number: true,
        string: true,
      },
    ],
    'no-param-reassign': [
      'error',
      {
        props: true,
        ignorePropertyModificationsFor: ['acc', 'accumulator', 'el', 'element', 'state'],
      },
    ],
    'no-sequences': [
      'error',
      {
        allowInParentheses: false,
      },
    ],
    'promise/catch-or-return': [
      'error',
      {
        allowFinally: true,
      },
    ],
    'vue/html-self-closing': [
      'error',
      {
        html: {
          void: 'any',
          normal: 'never',
          component: 'always',
        },
        svg: 'always',
        math: 'always',
      },
    ],
    'vue/component-tags-order': [
      'error',
      {
        order: ['script', 'template', 'style'],
      },
    ],
    'vue/component-name-in-template-casing': 'off',
    '@gitlab/vue-require-required-key': 'error',
    'no-restricted-globals': [
      'error',
      ...restrictedGlobals,
      {
        name: 'escape',
        message:
          "The global `escape` function is unsafe. Did you mean to use `encodeURI`, `encodeURIComponent` or lodash's `escape` instead?",
      },
      {
        name: 'unescape',
        message:
          "The global `unescape` function is unsafe. Did you mean to use `decodeURI`, `decodeURIComponent` or lodash's `unescape` instead?",
      },
    ],
    'vue/v-slot-style': [
      'error',
      {
        atComponent: 'shorthand',
      },
    ],
    '@gitlab/vue-no-data-toggle': 'error',
    '@gitlab/vue-slot-name-casing': 'error',
    '@gitlab/no-runtime-template-compiler': 'error',
    'import/order': [
      'error',
      {
        groups: ['builtin', 'external', 'internal', 'parent', 'sibling', 'index'],
      },
    ],
    // BEGIN rules to aid migration from Vue 2.x to 3.x.
    // See https://gitlab.com/groups/gitlab-org/-/epics/3174 for more details.
    'vue/no-deprecated-data-object-declaration': 'error',
    'vue/no-deprecated-events-api': 'error',
    'vue/no-deprecated-filter': 'error',
    'vue/no-deprecated-functional-template': 'error',
    'vue/no-deprecated-html-element-is': 'error',
    'vue/no-deprecated-inline-template': 'error',
    'vue/no-deprecated-props-default-this': 'error',
    'vue/no-deprecated-scope-attribute': 'error',
    'vue/no-deprecated-slot-attribute': 'error',
    'vue/no-deprecated-slot-scope-attribute': 'error',
    'vue/no-deprecated-v-on-number-modifiers': 'error',
    'vue/no-deprecated-vue-config-keycodes': 'error',
    // END rules to aid migration from Vue 2.x to 3.x.
  },
  overrides: [
    {
      files: ['**/*.vue'],
      rules: {
        'import/no-default-export': 'off',
      },
    },
  ],
};
