module.exports = {
  plugins: ['@gitlab'],
  rules: {
    '@gitlab/require-i18n-strings': 'error',
    '@gitlab/vue-require-i18n-attribute-strings': 'error',
    '@gitlab/vue-require-i18n-strings': 'error',
    '@gitlab/require-valid-i18n-helpers': 'error',
    '@gitlab/vue-require-valid-i18n-helpers': 'error',
  },
};
